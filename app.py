"""
 Flask REST application

"""
import string, random, db, os

from flask import Flask, session, request, jsonify, make_response, redirect
from flask_mail import Mail, Message

app = Flask(__name__, static_url_path='/static')
app.secret_key = os.urandom(24)

# EMAIL SETTINGS
app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=587,
    MAIL_USE_SSL=False,
    MAIL_USE_TLS=True,
    MAIL_USERNAME='projeto.unity.fixe@gmail.com',
    MAIL_PASSWORD='Ola12345'
)

# email init
mail = Mail(app)


"""--------------------------------EMAIL FUNCTIONS--------------------------------"""


def generate_confirmation_token(string_length=25):
    """Generate a random string of letters and digits to serve as the e-mail authentication token"""
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join(random.choice(letters_and_digits) for i in range(string_length))


def send_mail(email, confirmation_token):
    """Sends an e-mail with the confirmation token to the signed up user"""
    msg = Message(
        'Python Project E-mail Authentication, by Henoch and Pedro',
        sender='projeto.unity.fixe@gmail.com',
        recipients=[email])
    msg.html = "Click this link to verify your account in the awesome python REST project:\n" \
               "<a href='http://localhost:8000/api/verify/" + confirmation_token + "'> Confirm!</a>"
    mail.send(msg)
    return "Sent"


"""--------------------------------END POINTS--------------------------------"""


@app.route('/')
def root():
    if 'user' in session:
        return redirect('/app')
    return app.send_static_file('authentication/login.html')


@app.route('/api/verify/<string:token>')
def verify_mail(token):
    db.verify_user(token)
    return app.send_static_file('authentication/login.html')


@app.route('/app')
def application():
    if 'user' not in session:
        return redirect('/')
    return app.send_static_file('index.html')


@app.route('/register')
def register():
    return app.send_static_file('authentication/regist.html')


"""--------------------------------USER--------------------------------"""


@app.route("/api/user/", methods=['GET', 'PUT'])
def logged_user():
    if request.method == 'GET':
        user_id = session['id']
        user = db.get_user(user_id)
        return make_response(jsonify(user), 200)
    if request.method == 'PUT':
        data = request.get_json()
        db.update_user(data)
        return make_response(jsonify(), 200)
    return make_response(jsonify(), 400)


@app.route("/api/user/register/", methods=["POST"])
def register_user():
    if request.method == 'POST':
        data = request.get_json()
        if data['email'] == "":
            # not acceptable
            return make_response(jsonify(), 406)
        exists = db.check_email(data['email'])
        if exists:
            # bad request
            return make_response(jsonify(), 400)
        # generate random confirmation string
        data['token'] = generate_confirmation_token()
        user = db.add_user(data)
        send_mail(user['email'], data['token'])
        return make_response(jsonify(user), 201)
    return make_response(jsonify(), 400)


@app.route("/api/user/login/", methods=["POST"])
def login():
    if request.method == 'POST':
        user = request.get_json()
        existing_user = db.check_for_login(user)
        # check user's existence
        if existing_user is not None:
            user_status = db.get_user_verified_status(existing_user['id'])
            # check if user confirmed the e-mail
            if user_status['confirmed'] == 'True':
                session['user'] = existing_user['username']
                session['id'] = existing_user['id']
                return make_response(jsonify(user), 200)
            else:
                # Unauthorized User 401
                return make_response(jsonify(), 401)
        # Forbidden 403 Site Access denied .8
        return make_response(jsonify(), 403.8)
    return make_response(jsonify(), 400)


@app.route("/api/user/logout/")
def logout():
    if 'user' in session:
        session.pop('user', None)
        return make_response(jsonify(), 200)
    else:
        return make_response(jsonify(), 400)


"""--------------------------------PROJECT--------------------------------"""


@app.route("/api/projects/", methods=["GET", "POST"])
def get_projects():
    user_id = session['id']
    if request.method == "GET":
        projects = db.get_projects(user_id)
        return make_response(jsonify(projects), 200)
    elif request.method == "POST":
        data = request.get_json()
        db.add_project(user_id, data)
        return make_response(jsonify(), 200)
    return make_response(jsonify(), 400)


@app.route("/api/projects/<int:pk>/", methods=["GET", "PUT", "DELETE"])
def get_project(pk):
    if request.method == 'GET':
        project = db.get_project(pk)
        return make_response(jsonify(project), 200)
    elif request.method == 'PUT':
        data = request.get_json()
        db.update_project(data)
        project = db.get_project(pk)
        return make_response(jsonify(project), 200)
    elif request.method == 'DELETE':
        db.delete_project(pk)
        return make_response("The project with the id '" + str(pk) + "' was deleted", 200)
    return make_response(jsonify(), 404)


"""--------------------------------TASK--------------------------------"""


@app.route("/api/projects/<int:protect_id>/tasks/", methods=["GET"])
def get_project_tasks(protect_id):
    if request.method == 'GET':
        tasks = db.get_project_tasks(protect_id)
        return make_response(jsonify(tasks), 200)
    return make_response(jsonify(), 400)


@app.route("/api/projects/<int:protect_id>/tasks/<int:task_id>/", methods=["GET", "PUT", "DELETE"])
def get_task(protect_id, task_id):
    if request.method == 'GET':
        task = db.get_task(protect_id, task_id)
        return make_response(jsonify(task), 200)
    elif request.method == 'PUT':
        data = request.get_json()
        db.update_task(task_id, data)
        task = db.get_task(protect_id, task_id)
        return make_response(jsonify(task), 200)
    elif request.method == 'DELETE':
        db.delete_task(task_id)
        return make_response("The task with the id '" + str(task_id) + "' was deleted", 200)
    return make_response(jsonify(), 400)


@app.route("/api/projects/<int:protect_id>/task/", methods=["POST"])
def new_task(protect_id):
    if request.method == 'POST':
        data = request.get_json()
        task = db.create_task(protect_id, data)
        return make_response(jsonify(task), 200)
    return make_response(jsonify(), 400)


db.recreate_db()
app.run(host='0.0.0.0', port=8000, debug=True)

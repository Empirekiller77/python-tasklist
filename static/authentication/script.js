function register(){
    var email = document.getElementById("email_input").value;
    var password = document.getElementById("pass_input").value;
    var req = new XMLHttpRequest();
    req.open("POST", "/api/user/register/");
    req.setRequestHeader("Content-Type", "application/json");
    req.addEventListener("load", function() {
        if(req.status == '201'){
            alert('You signed up with success! Check your email to confirm your account.');
            window.location.href = '/'
        }else if(req.status == '400'){
            alert('There already is an account with this E-mail!');
        }else{
            alert('Something went wrong, please try again!');
        }
    });
    req.send(JSON.stringify({"name": null, "email": email, "username": null, "password": password}));
    document.getElementById("email_input").value = '';
    document.getElementById("pass_input").value = '';
};

function login(){
    var email = document.getElementById("email_input").value;
    var password = document.getElementById("pass_input").value;
    var req = new XMLHttpRequest();
    req.open("POST", "/api/user/login/");
    req.setRequestHeader("Content-Type", "application/json");
    req.addEventListener("load", function() {
        if(req.status == '403')
            document.getElementById('error').style.display = "block";
        else if(req.status == '200'){
            window.location.href = "/app";
        }else if(req.status == '401'){
            alert('User is not Verified! Check your Email.')
        }

    });
    req.send(JSON.stringify({"email": email, "password": password}));
    document.getElementById("email_input").value = '';
    document.getElementById("pass_input").value = '';
}

function logout(){
    var req = new XMLHttpRequest();
    req.open("GET", "/api/user/logout/");
    req.addEventListener("load", function() {
        if(req.status == '200'){
            window.location.href = "/";
        }else if(req.status == '400'){
            window.location.href = "/static/erros/400.html";
        }

    });
    req.send();
}
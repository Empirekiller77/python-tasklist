function getProjects() {
    var req = new XMLHttpRequest();
    req.open("GET", "/api/projects/");
    req.addEventListener("load", function() {
        var projects = JSON.parse(this.responseText);
        var ul = document.getElementById('projects');
        ul.innerHTML = '';
        for (var i in projects) {
            var li = document.createElement('li');
            li.id = projects[i].id;
            li.classList.add("list-group-item");
            li.innerHTML = projects[i].title + ' (' + projects[i].creation_date + ')';
            li.innerHTML += " <input type='image' src='/static/authentication/images/icons/update_icon.svg' onclick='getTasks(" + projects[i].id + ")' data-toggle='tooltip' data-placement='top' title='Update'>";
            li.innerHTML += " <input type='image' src='/static/authentication/images/icons/delete_icon.svg' onclick='deleteProject(" + projects[i].id + ")' data-toggle='tooltip' data-placement='top' title='Delete'>";
            ul.appendChild(li);
        }
    });
    req.send();
}
function getCurrentDate(){
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}
function getTasks(project_id){
    var req = new XMLHttpRequest();
    req.open("GET", "/api/projects/"+project_id+"/tasks");
    var ul = document.getElementById('tasks');
    ul.innerHTML = "";
    req.addEventListener("load", function() {
        var today = getCurrentDate();
        var tasks = JSON.parse(this.responseText);
        for(var i in tasks){
            var li = document.createElement('li');
            if(tasks[i].due_date < today)
                li.classList.add("overdue");
            if(tasks[i].completed == "True")
                li.classList.add("completed");
            li.classList.add("list-group-item");
            li.innerHTML = tasks[i].title + " (" +tasks[i].creation_date +") - " + "(" + tasks[i].due_date + ")";
            li.innerHTML += ' <input onclick="getTask('+ project_id +','+ tasks[i].id +')" type="image" src="/static/authentication/images/icons/open_modal_icon.svg" style="width:20px;height:20px" data-toggle="modal" data-target="#exampleModal"/>';
            li.innerHTML += " &nbsp;<input type='image' src='/static/authentication/images/icons/delete_icon.svg' onclick='deleteTask(" + project_id + ", "+ tasks[i].id +")' data-toggle='tooltip' data-placement='top' title='Delete'>";
            ul.appendChild(li);
        }
    });
    getProject(project_id)
    req.send();
}

function getTask(project_id, task_id){
    document.getElementById("task_creation_date_form_group").style.display = "block";
    var req = new XMLHttpRequest();
    req.open("GET", "/api/projects/"+project_id+"/tasks/" + task_id +"/");
    var div = document.getElementById('task_body');
    req.addEventListener("load", function() {
        var task = JSON.parse(this.responseText);
        document.getElementById("title_task").value = task.title;
        document.getElementById("order_task").value = task.order;
        document.getElementById("task_creation_date").value = task.creation_date;
        document.getElementById("task_due_date").value = task.due_date;
        document.getElementById("saveTask").innerHTML = '<button onclick="updateTask(' + task.id +','+ project_id +')" type="button" class="btn btn-primary">Save changes</button>';
        if(task.completed == "True")
           document.getElementById("task_completed").checked = true;
        else
           document.getElementById("task_completed").checked = false;
    });
    getProject(project_id);
    req.send();
}

function updateTask(task_id, project_id){
    var title = document.getElementById("title_task").value;
    if(title == "" || title == " "){
        alert("Fill all the fields!");
    }else{
        var order = document.getElementById("order_task").value;
        var creation_date = document.getElementById("task_creation_date").value;
        var due_date = document.getElementById("task_due_date").value;
        if(document.getElementById("task_completed").checked)
            var completed = "True";
        else
            var completed = "False";
        var req = new XMLHttpRequest();
        req.open("PUT", "/api/projects/"+project_id+"/tasks/"+task_id+"/");
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify({"id": task_id, "title": title, "creation_date": creation_date, "order": order, "due_date": due_date, "completed": completed}));
        req.addEventListener("load", function() {
            if(req.status == "200"){
                alert("Task edited!");
                document.getElementById("close").click();
            }
            getTasks(project_id);
        });
    }
}

function getProject(project_id){
    var req = new XMLHttpRequest();
    req.open("GET", "/api/projects/"+project_id+"/");
    req.addEventListener("load", function() {
        var project = JSON.parse(this.responseText);
        document.getElementById("title").value = project.title;
        document.getElementById("creation_date").value = project.creation_date;
        document.getElementById("buttonUpdateProject").innerHTML = '<button type="button" class="btn btn-success" onclick="updateProject('+project_id+')">Save</button>';
        document.getElementById("CreateTask").innerHTML = '<button type="button" class="btn btn-primary" onclick="getTaskModal('+project_id+')" data-toggle="modal" data-target="#exampleModal">Create Task</button>';
    });
    req.send();
}


function getTaskModal(project_id){
    document.getElementById("saveTask").innerHTML = '<button onclick="addTask(' + project_id + ')" type="button" class="btn btn-primary">Save</button>';
    document.getElementById("title_task").value = "";
    document.getElementById("order_task").value = "";
    document.getElementById("task_creation_date_form_group").style.display = "none";
    document.getElementById("task_due_date").value = "dd-mm-aaaa";
    document.getElementById("task_completed").checked = false;
}


function addTask(project_id){
    var title = document.getElementById("title_task").value;
    var order = document.getElementById("order_task").value;
    var due_date = document.getElementById("task_due_date").value;
    if(document.getElementById("task_completed").checked)
        var completed = "True";
    else
        var completed = "False";
    var req = new XMLHttpRequest();
    req.open("POST", "/api/projects/"+project_id+"/task/");
    req.setRequestHeader("Content-Type", "application/json");
    req.send(JSON.stringify({"id": project_id, "title": title, "order": order, "due_date": due_date, "completed": completed}));
    req.addEventListener("load", function() {
        var response = JSON.parse(this.responseText);
        if(req.status == "200"){
            alert("Task added to the project!");
            document.getElementById("close").click();
        }
        getTasks(project_id);
    });
}


function updateProject(project_id){
    var title = document.getElementById("title").value;
    if (title=="" || title==" "){
        alert("Fill all the fields!");
    }else{
        var creation_date = document.getElementById("creation_date").value;
        var req = new XMLHttpRequest();
        req.open("PUT", "/api/projects/"+project_id+"/");
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify({"id": project_id, "title": title, "creation_date": creation_date}));

        req.addEventListener("load", function() {
            var response = JSON.parse(this.responseText);
            if(req.status == "200"){
                alert("Projeto editado!");
                document.getElementById("close").click();
            }
            getProject(project_id);
            getProjects();
        });
    }
}

function deleteProject(project_id) {
    var req = new XMLHttpRequest();
    req.open("DELETE", "/api/projects/" + project_id + "/");
    req.addEventListener("load", function() {
        if(req.status == "200"){
            alert("Project was deleted successfully!");
        }
        getProjects();
    });
    req.send();
}

function deleteTask(project_id, task_id){
    var req = new XMLHttpRequest();
    req.open("DELETE", "/api/projects/" + project_id + "/tasks/" + task_id + "/");
    req.addEventListener("load", function() {
        if(req.status == "200"){
            alert("Task was deleted successfully!");
        }
        getTasks(project_id);
    });
    req.send();

}
function mountUserModal(){
    document.getElementById("modal_body").innerHTML='<div class="form-group"><label for="name">Name:</label>'+
    '<input type="text" class="form-control" id="user_name"></div>'+
    '<div class="form-group"><label for="email">Email:</label>'+
    '<input type="text" class="form-control" id="user_email" data-validate = "Valid email is: a@b.c"></div>'+
    '<div class="form-group"><label for="password">Password:</label>'+
    '<input type="password" class="form-control" id="user_password"></div>'+
    '<div class="form-group"><label for="username">UserName:</label>'+
    '<input type="text" class="form-control" id="user_username"></div>';
    getUserInfo();
}

function getUserInfo(){
    var req = new XMLHttpRequest();
    req.open("GET", "api/user/");
    req.addEventListener("load", function() {
        var user = JSON.parse(this.responseText);
        document.getElementById("user_name").value = user.name;
        document.getElementById("user_email").value = user.email;
        document.getElementById("user_password").value = user.password;
        document.getElementById("user_username").value = user.username;
        document.getElementById("saveModal").innerHTML='<button onclick="updateUser('+user.id+')" type="button" class="btn btn-primary">Save changes</button>'
    });
    req.send();
}

function updateUser(user_id){
    var name = document.getElementById("user_name").value;
    var email = document.getElementById("user_email").value;
    var password = document.getElementById("user_password").value;
    var username = document.getElementById("user_username").value;
    if(name == "" || name == " " || email == "" || email == " " || password == "" ||
    password == " " || username == "" || username == " "){
        alert('Fill all fields!');
        }else{

        var req = new XMLHttpRequest();
        req.open("PUT", "/api/user/");
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify({"id": user_id, "name": name, "email": email, "password": password, "username": username}));
        req.addEventListener("load", function() {
            if(req.status == 200)
                alert("User's information was updated!");
            else
                alert(req.statusText);
        });
    }
}

function mountCreateProjectModal(){
    document.getElementById("modal_body").innerHTML='<div class="form-group"><label for="project_title">title:</label>'+
    '<input type="text" class="form-control" id="project_title"></div>';
    document.getElementById("saveModal").innerHTML='<button onclick="createProject()" type="button" class="btn btn-primary">Save changes</button>'
}

function createProject(){
    var title = document.getElementById("project_title").value;
    if(title =="" || title ==" "){
        alert('Fill all fields!');
    }else{

        var req = new XMLHttpRequest();
        req.open("POST", "/api/projects/");
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify({"title": title}));
        req.addEventListener("load", function() {
            if(req.status == 200)
                alert("Project was created!");
            else
                alert(req.statusText);

            document.getElementById("closeModalDefault").click();
            getProjects();
        });
    }
}

getProjects();

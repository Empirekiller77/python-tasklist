"""
 Implements a simple database of users.

"""

import sqlite3
from datetime import date

conn = sqlite3.connect('database.db', check_same_thread=False)


def dict_factory(cursor, row):
    """Converts table row to dictionary."""
    res = {}
    for idx, col in enumerate(cursor.description):
        res[col[0]] = row[idx]
    return res


conn.row_factory = dict_factory


def recreate_db():
    """Recreates the database."""
    c = conn.cursor()
    c.execute("DROP TABLE IF EXISTS user")
    c.execute("DROP TABLE IF EXISTS project")
    c.execute("DROP TABLE IF EXISTS task")
    c.execute("""
        CREATE TABLE IF NOT EXISTS user (
            id INTEGER PRIMARY KEY,
            name TEXT NULL,
            email TEXT,
            username TEXT NULL,
            password TEXT,
            confirmed BOOLEAN,
            confirmation_code TEXT NULL
        )
    """)
    c.execute("""
        CREATE TABLE IF NOT EXISTS project (
            id INTEGER PRIMARY KEY,
            user_id INTEGER,
            title TEXT,
            creation_date TEXT,
            last_updated TEXT,
            FOREIGN KEY(user_id) REFERENCES user(id)
        )
    """)
    c.execute("""
        CREATE TABLE IF NOT EXISTS task (
            id INTEGER PRIMARY KEY,
            project_id INTEGER,
            title TEXT,
            "order" INTEGER,
            creation_date TEXT,
            due_date TEXT,
            completed BOOLEAN,
            FOREIGN KEY(project_id) REFERENCES project(id)
        )
    """)
    c.execute("INSERT INTO user VALUES (null, 'admin', 'admin@admin.pt', 'admin', 'admin', 'True', null)")
    c.execute("INSERT INTO user VALUES (null, 'teste', 'teste@teste.pt', 'teste', 'teste', 'True', null)")
    c.execute("INSERT INTO project VALUES (null, 1, 'Teste', '2019-06-01', '2019-06-01')")
    c.execute("INSERT INTO project VALUES (null, 1, 'Vazio', '2019-06-05', '2019-06-05')")
    c.execute("INSERT INTO project VALUES (null, 1, 'teste2', '2019-06-05', '2019-06-05')")
    c.execute("INSERT INTO task VALUES (null, 1, 'taskTeste', 1, '2019-06-01', '2019-06-03', 'True')")
    c.execute("INSERT INTO task VALUES (null, 1, 'taskTeste2', 2, '2019-06-05', '2019-06-16', 'False')")
    c.execute("INSERT INTO task VALUES (null, 3, 'teste22', 1, '2019-06-01', '2019-07-08', 'True')")
    c.execute("INSERT INTO task VALUES (null, 3, 'taskTeste', 2, '2019-06-01', '2019-05-03', 'False')")
    conn.commit()


def check_email(email):
    stmt = 'SELECT * FROM user WHERE email="%s"' % email
    res = conn.cursor().execute(stmt)
    if res.fetchone() is not None:
        return True
    return False


def check_for_login(user):
    email = user['email']
    psw = user['password']
    stmt = 'SELECT * FROM user WHERE email="%s" AND password="%s"' % (email, psw)
    res = conn.cursor().execute(stmt)
    return res.fetchone()


def get_user(pk):
    """Returns a single user."""
    res = conn.cursor().execute('SELECT * FROM user WHERE id=%s' % pk)
    return res.fetchone()


def add_user(user):
    """Adds a new user."""
    print("print token in BD: " + user['token'])
    stmt = "INSERT INTO user VALUES (null, '%s', '%s', '%s', '%s', 'False', '%s')" % (user['name'], user['email'],
            user['username'], user['password'], user['token'])
    c = conn.cursor()
    c.execute(stmt)
    conn.commit()
    return get_user(c.lastrowid)


def update_user(data):
    """Updates a user with the given data."""
    stmt = "UPDATE user SET name='%s', email='%s', password='%s', username='%s' WHERE id=%d" % (
        data['name'], data['email'], data['password'], data['username'], data['id'])
    conn.cursor().execute(stmt)
    conn.commit()


def remove_user(user):
    """Deletes a user."""
    stmt = "DELETE FROM user WHERE id=%s" % user['id']
    conn.cursor().execute(stmt)
    conn.commit()


def verify_user(token):
    """Updates a user's verified status."""
    stmt = "UPDATE user SET confirmed='True', confirmation_code=null WHERE confirmation_code='%s'" % (
            token)
    conn.cursor().execute(stmt)
    conn.commit()


def get_user_verified_status(user_id):
    """Returns a single user's verified status."""
    res = conn.cursor().execute('SELECT confirmed FROM user WHERE id=%d' % user_id)
    return res.fetchone()


def add_project(user_id, data):
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    stmt = "INSERT INTO project VALUES (null, %d, '%s', '%s', '%s')" % (user_id,data['title'], d1, d1)
    conn.cursor().execute(stmt)
    conn.commit()


def get_projects(user_id):
    cur = conn.cursor()
    cur.execute("SELECT * FROM project WHERE user_id=%d order by last_updated desc" % user_id)
    rows = cur.fetchall()
    return rows


def get_project(pk):
    stmt = "SELECT * FROM project WHERE id=%d" % pk
    res = conn.cursor().execute(stmt)
    conn.commit()
    return res.fetchone()


def update_project(data):
    """Updates a project with the given data."""
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    stmt = "UPDATE project SET title='%s', creation_date='%s', last_updated='%s' WHERE id=%d" % (
        data['title'], data['creation_date'], d1, data['id'])
    conn.cursor().execute(stmt)
    conn.commit()


def delete_project(project_id):
    stmt = "DELETE FROM project WHERE id=%d" % project_id
    conn.cursor().execute(stmt)
    conn.commit()


def get_project_tasks(project_id):
    cur = conn.cursor()
    cur.execute("SELECT * FROM task WHERE project_id=%d" % project_id)
    rows = cur.fetchall()
    return rows


def get_task(project_id, task_id):
    cur = conn.cursor()
    cur.execute("SELECT * FROM task WHERE project_id=%d AND id =%d" % (project_id, task_id))
    rows = cur.fetchone()
    return rows


def create_task(project_id, data):
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    stmt = "INSERT INTO task VALUES (null, '%d', '%s', '%d', '%s', '%s', '%s')" % (project_id, data['title'],
                                                                                   int(data['order']), d1,
                                                                                   data['due_date'], data['completed'])
    c = conn.cursor()
    c.execute(stmt)
    conn.commit()
    return get_task(project_id, c.lastrowid)


def update_task(task_id, data):
    stmt = "UPDATE task SET title='%s', [order]=%d, due_date='%s', completed='%s' WHERE id=%d" % (
        data['title'], int(data['order']), data['due_date'], data['completed'], task_id)
    res = conn.cursor().execute(stmt)
    conn.commit()


def delete_task(task_id):
    stmt = "DELETE FROM task WHERE id=%d" % task_id
    conn.cursor().execute(stmt)
    conn.commit()
